import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";
import axios from "axios";

class Settings extends Component {
  state = {
    id: "",
    procentricServerIp: "",
    procentricServerPort: "9934",
    openSnackbar: false,
    message: "",
    triggerOn: false
  };

  componentDidMount() {
    axios.get(`/settings`).then(res => {
      const settings = res.data;
      console.log(settings);
      if (settings === undefined || settings.length === 0) {
        // settings empty or does not exist
        return;
      }
      const id = settings[0]._id;
      const pcip = settings[0].procentricServerIp;
      const pcport = settings[0].procentricServerPort;
      //console.log(rows);
      this.setState({ id: id, procentricServerIp: pcip, procentricServerPort: pcport });
    });
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleSettingsSave = () => {
    const { id, procentricServerIp, procentricServerPort } = this.state;
    console.log(id, procentricServerIp, procentricServerPort);
    if (id === "") {
      axios
        .post(`/settings`, {
          procentricServerType: "PCA",
          procentricServerIp: procentricServerIp,
          procentricServerPort: procentricServerPort
        })
        .then(res => {
          console.log(res.data);
          this.setState({ message: "Setting is saved.", openSnackbar: true, id: res.data[0]._id });
        });
    } else {
      axios
        .put(`/settings/${id}`, {
          procentricServerIp: procentricServerIp,
          procentricServerPort: procentricServerPort
        })
        .then(res => {
          console.log(res.data);
          this.setState({ message: "Setting is saved.", openSnackbar: true });
        });
    }
  };

  handleSettingsTrigger = () => {
    const { id, procentricServerIp, procentricServerPort } = this.state;
    console.log(id, procentricServerIp, procentricServerPort);
    // console.log("http://" + procentricServerIp + ":" + procentricServerPort + "/adminClient/execute?command=updatePMS");
    if (id !== "") {
      this.setState({ triggerOn: true });
      fetch(`http://${procentricServerIp}:${procentricServerPort}/adminClient/execute?command=updatePMS`, {
        mode: "no-cors"
      })
        .then(res => {
          console.log(res.data);
          this.setState({ message: "Fetching succeeded.", openSnackbar: true });
          this.setState({ triggerOn: false });
        })
        .catch(error => {
          console.warn("Not good man :(");
          this.setState({ message: "Fetching is failed.", openSnackbar: true });
          this.setState({ triggerOn: false });
        });
    } else {
      console.warn("Save valid data first!");
      this.setState({ message: "Save valid data first!", openSnackbar: true });
    }
  };
  handleSnackbarClose = () => {
    this.setState({ openSnackbar: false });
  };

  render() {
    const { openSnackbar, message, triggerOn } = this.state;
    return (
      <div>
        <Grid container spacing={0} alignItems="center" justify="center" style={{ margin: "20px" }}>
          <Grid item xs={6}>
            <Paper style={{ padding: "20px" }}>
              <Typography variant="h6" color="inherit">
                Pro:Centric Server configuration
              </Typography>
              <TextField
                autoFocus
                margin="dense"
                required
                id="standard-required"
                label="IP Address"
                value={this.state.procentricServerIp}
                onChange={this.handleChange("procentricServerIp")}
                fullWidth
              />
              <TextField
                margin="dense"
                disabled
                id="standard-required"
                label="Port Number"
                value={this.state.procentricServerPort}
                onChange={this.handleChange("procentricServerPort")}
                fullWidth
              />
              <Grid container justify="flex-end">
                <Grid item>
                  <Button onClick={this.handleSettingsTrigger} color="primary">
                    {triggerOn === false && "Trigger"}
                    {triggerOn === true && <CircularProgress size={20} />}
                  </Button>
                  <Button onClick={this.handleSettingsSave} color="primary">
                    Save
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          open={openSnackbar}
          autoHideDuration="3000"
          onClose={this.handleSnackbarClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{message}</span>}
        />
      </div>
    );
  }
}

export default Settings;
