import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import PmsSim from "./PmsSim";
import Settings from "./Settings";
import { AuthService } from "../App";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

class MenuAppBar extends React.Component {
  state = {
    value: 0,
  };

  handlePmsSim = () => {
    this.setState({ value: 0 });
  }
  handleSettings = () => {
    this.setState({ value: 1 });
  };
  handleLogout = () => {
    //tba...
    AuthService.logout(() => this.props.history.push("/"));
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Button color="inherit" onClick={this.handlePmsSim}>
              <Typography variant="h6" color="inherit">
                Simple PMS for PCA/PCD
              </Typography>
            </Button>
            <Typography variant="h6" color="inherit" className={classes.grow}>
            </Typography>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.handleSettings}>
              <SettingsIcon />
            </IconButton>
            <Button color="inherit" onClick={this.handleLogout}>Logout</Button>
          </Toolbar>
        </AppBar>
        {value === 0 && <PmsSim/>}
        {value === 1 && <Settings/>}
      </div>
    );
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  withRouter
)(MenuAppBar);