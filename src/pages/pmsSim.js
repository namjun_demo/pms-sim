import * as React from "react";
import {
  SearchState,
  SortingState,
  EditingState,
  PagingState,
  IntegratedPaging,
  IntegratedSorting,
  IntegratedFiltering
} from "@devexpress/dx-react-grid";
import {
  Grid,
  Table,
  TableHeaderRow,
  TableEditRow,
  TableEditColumn,
  PagingPanel,
  DragDropProvider,
  TableColumnReordering,
  TableFixedColumns,
  Toolbar,
  SearchPanel
} from "@devexpress/dx-react-grid-material-ui";
import Paper from "@material-ui/core/Paper";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import TableCell from "@material-ui/core/TableCell";

import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import Snackbar from "@material-ui/core/Snackbar";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";

const styles = theme => ({
  root: {
    margin: "1%"
  },
  dialog: {
    width: "calc(100% - 16px)"
  },
  inputRoot: {
    width: "100%"
  }
});

const AddButton = ({ onExecute }) => (
  <div style={{ textAlign: "center" }}>
    <Button color="primary" onClick={onExecute} title="Create new row">
      Add Room
    </Button>
  </div>
);

const EditButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Edit room">
    <EditIcon />
  </IconButton>
);

const DeleteButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Remove room">
    <DeleteIcon />
  </IconButton>
);

const CommitButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Save changes">
    <SaveIcon />
  </IconButton>
);

const CancelButton = ({ onExecute }) => (
  <IconButton color="secondary" onClick={onExecute} title="Cancel changes">
    <CancelIcon />
  </IconButton>
);

const commandComponents = {
  add: AddButton,
  edit: EditButton,
  delete: DeleteButton,
  commit: CommitButton,
  cancel: CancelButton
};

const Command = ({ id, onExecute }) => {
  const CommandButton = commandComponents[id];
  return <CommandButton onExecute={onExecute} />;
};

const availableValues = {
  checkInOut: ["Check Out", "Check In"]
};

const LookupEditCellBase = ({ availableColumnValues, value, onValueChange, classes }) => (
  <TableCell className={classes.lookupEditCell}>
    <Select value={value} onChange={event => onValueChange(event.target.value)} input={<Input classes={{ root: classes.inputRoot }} />}>
      {availableColumnValues.map(item => (
        <MenuItem key={item} value={item}>
          {item}
        </MenuItem>
      ))}
    </Select>
  </TableCell>
);
export const LookupEditCell = withStyles(styles, { name: "ControlledModeDemo" })(LookupEditCellBase);

const Cell = props => {
  return <Table.Cell {...props} />;
};

const EditCell = props => {
  const { column } = props;
  const availableColumnValues = availableValues[column.name];
  if (availableColumnValues) {
    return <LookupEditCell {...props} availableColumnValues={availableColumnValues} />;
  }
  return <TableEditRow.Cell {...props} />;
};

const getRowId = row => row.id;

class PmsSim extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        { name: "checkInOut", title: "Check In/Out", align: "center" },
        { name: "roomNumber", title: "Room Number", align: "center" },
        { name: "customer", title: "Customer", align: "center" },
        { name: "checkInDate", title: "Check In/Out Date", align: "center" }
      ],
      tableColumnExtensions: [
        { columnName: "checkInOut", width: 200, align: "center" },
        { columnName: "roomNumber", width: 180, align: "center" },
        { columnName: "customer", width: 300, align: "center" },
        { columnName: "checkInOutDate", width: 200, align: "center" }
      ],
      rows: [],
      sorting: [],
      editingRowIds: [],
      addedRows: [],
      rowChanges: {},
      currentPage: 0,
      deletingRows: [],
      pageSize: 0,
      pageSizes: [5, 10, 0],
      columnOrder: ["checkInOut", "roomNumber", "customer", "checkInDate"],
      leftFixedColumns: [TableEditColumn.COLUMN_TYPE],
      openSnackbar: false,
      message: "",
      snackbarTimeout: null,
      id: "",
      procentricServerIp: "",
      procentricServerPort: ""
    };
    //console.log(this.state.rows);
    const getStateDeletingRows = () => {
      const { deletingRows } = this.state;
      return deletingRows;
    };
    const getStateRows = () => {
      const { rows } = this.state;
      return rows;
    };

    this.changeSorting = sorting => this.setState({ sorting });
    this.changeEditingRowIds = editingRowIds => this.setState({ editingRowIds });
    this.changeAddedRows = addedRows =>
      this.setState({
        addedRows: addedRows.map(row =>
          Object.keys(row).length
            ? row
            : {
                checkInDate: new Date().toISOString().split("T")[0],
                checkInOut: "Check Out",
                roomNumber: "101",
                customer: "John Smith"
              }
        )
      });
    this.changeRowChanges = rowChanges => this.setState({ rowChanges });
    this.changeCurrentPage = currentPage => this.setState({ currentPage });
    this.changePageSize = pageSize => this.setState({ pageSize });
    this.commitChanges = ({ added, changed, deleted }) => {
      let { rows } = this.state;
      if (added) {
        axios
          .post(`/rooms`, {
            checkInOut: added[0].checkInOut,
            roomNumber: added[0].roomNumber,
            customer: added[0].customer
          })
          .then(res => {
            console.log(res);
            console.log(res.data);
            rows = [
              ...rows,
              ...added.map((row, index) => ({
                id: res.data._id,
                ...row
              }))
            ];
            this.setState({ rows, deletingRows: deleted || getStateDeletingRows() });
          });
      }
      if (changed) {
        //console.log(changed);
        //console.log(Object.values(changed)[0]);
        axios.put(`/rooms/${Object.keys(changed)[0]}`, Object.values(changed)[0]).then(res => {
          console.log(res);
          console.log(res.data);
          rows = rows.map(row => (changed[row.id] ? { ...row, ...changed[row.id], checkInDate: new Date().toISOString().split("T")[0] } : row));
          this.setState({ rows, deletingRows: deleted || getStateDeletingRows() });

          this.setState({ message: "Trigger PCA fetching new data...", openSnackbar: true });
          const { id, procentricServerIp, procentricServerPort } = this.state;
          if (id !== "") {
            fetch(`http://${procentricServerIp}:${procentricServerPort}/adminClient/execute?command=updatePMS`, {
              mode: "no-cors"
            })
              .then(res => {
                console.log(res.data);
                console.log();
                this.setState({ message: "Fetch succeeded.", openSnackbar: true });
              })
              .catch(error => {
                console.error("Not good man :( ", error);
                this.setState({ message: "Fetching is failed.", openSnackbar: true });
              });
          } else {
            console.warn("Save valid data first!");
            this.setState({ message: "Save valid data first!", openSnackbar: true });
          }
        });
      }
      if (deleted) {
        this.setState({ rows, deletingRows: deleted || getStateDeletingRows() });
      }
    };
    this.cancelDelete = () => this.setState({ deletingRows: [] });
    this.deleteRows = () => {
      const rows = getStateRows().slice();
      getStateDeletingRows().forEach(rowId => {
        const index = rows.findIndex(row => row.id === rowId);
        console.log(index);
        console.log(rowId);
        if (index > -1) {
          axios.delete(`/rooms/${rowId}`).then(res => {
            console.log(res);
            console.log(res.data);
            rows.splice(index, 1);
            this.setState({ rows, deletingRows: [] });
          });
        }
      });
    };
    this.changeColumnOrder = order => {
      this.setState({ columnOrder: order });
    };
  }

  handleSnackbarClose = () => {
    this.setState({ openSnackbar: false });
  };

  componentDidMount() {
    axios.get(`/rooms`).then(res => {
      const rooms = res.data;
      //console.log(rooms);
      const rows = rooms.map(function(room) {
        var row = {};
        row.id = room._id;
        row.checkInOut = room.checkInOut;
        row.roomNumber = room.roomNumber;
        row.customer = room.customer;
        row.checkInDate = room.updatedAt.split("T")[0];
        return row;
      });
      //console.log(rows);
      this.setState({ rows });
    });
    axios.get(`/settings`).then(res => {
      const settings = res.data;
      console.log(settings);
      if (settings === undefined || settings.length === 0) {
        // settings empty or does not exist
        return;
      }
      const id = settings[0]._id;
      const pcip = settings[0].procentricServerIp;
      const pcport = settings[0].procentricServerPort;
      //console.log(rows);
      this.setState({ id: id, procentricServerIp: pcip, procentricServerPort: pcport });
    });
  }

  render() {
    const { classes } = this.props;
    const {
      rows,
      columns,
      tableColumnExtensions,
      sorting,
      editingRowIds,
      addedRows,
      rowChanges,
      currentPage,
      deletingRows,
      pageSize,
      pageSizes,
      columnOrder,
      leftFixedColumns,
      openSnackbar,
      message
    } = this.state;
    //console.log(rows);

    return (
      <Paper className={classes.root}>
        <Grid rows={rows} columns={columns} getRowId={getRowId}>
          <SearchState defaultValue="" />
          <IntegratedFiltering />
          <SortingState sorting={sorting} onSortingChange={this.changeSorting} />
          <PagingState
            currentPage={currentPage}
            onCurrentPageChange={this.changeCurrentPage}
            pageSize={pageSize}
            onPageSizeChange={this.changePageSize}
          />
          <EditingState
            editingRowIds={editingRowIds}
            onEditingRowIdsChange={this.changeEditingRowIds}
            rowChanges={rowChanges}
            onRowChangesChange={this.changeRowChanges}
            addedRows={addedRows}
            onAddedRowsChange={this.changeAddedRows}
            onCommitChanges={this.commitChanges}
          />

          <IntegratedSorting />
          <IntegratedPaging />

          <DragDropProvider />

          <Table columnExtensions={tableColumnExtensions} cellComponent={Cell} />
          <TableColumnReordering order={columnOrder} onOrderChange={this.changeColumnOrder} />
          <TableHeaderRow showSortingControls />
          <Toolbar />
          <SearchPanel />
          <TableEditRow cellComponent={EditCell} />
          <TableEditColumn width={170} showAddCommand={!addedRows.length} showEditCommand showDeleteCommand commandComponent={Command} />
          <TableFixedColumns leftColumns={leftFixedColumns} />
          <PagingPanel pageSizes={pageSizes} />
        </Grid>

        <Dialog open={!!deletingRows.length} onClose={this.cancelDelete} classes={{ paper: classes.dialog }}>
          <DialogTitle>Delete Room</DialogTitle>
          <DialogContent>
            <DialogContentText>Are you sure you want to delete the following room?</DialogContentText>
            <Paper>
              <Grid rows={rows.filter(row => deletingRows.indexOf(row.id) > -1)} columns={columns}>
                <Table columnExtensions={tableColumnExtensions} cellComponent={Cell} />
                <TableHeaderRow />
              </Grid>
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.cancelDelete} color="primary">
              Cancel
            </Button>
            <Button onClick={this.deleteRows} color="secondary">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          open={openSnackbar}
          autoHideDuration="3000"
          onClose={this.handleSnackbarClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{message}</span>}
        />
      </Paper>
    );
  }
}

export default withStyles(styles, { name: "ControlledModeDemo" })(PmsSim);
