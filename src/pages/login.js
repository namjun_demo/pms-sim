import React from "react";
import { Paper, withStyles, Grid, TextField, Button } from "@material-ui/core";
import { Face, Fingerprint } from "@material-ui/icons";
import { AuthService } from "../App";
import { Redirect } from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const styles = theme => ({
  margin: {
    margin: theme.spacing.unit * 2
  },
  padding: {
    padding: theme.spacing.unit
  }
});

class Login extends React.Component {
  state = {
    username: "",
    password: "",
    error: false,
    redirectToPreviousRoute: false,
    open: false
  };

  handleChange = name => event => {
    //console.log(event.target.value);
    this.setState({
      [name]: event.target.value
    });
  };
  handleClick = () => {
    const { username, password } = this.state;
    console.log(username, password);

    if (!(username === "admin" && password === "procentric")) {
      this.setState({ open: true });
      return;
    }

    AuthService.authenticate(() => {
      this.setState({ redirectToPreviousRoute: true });
    });
    //this.props.handleLogin(true);
    //this.props.history.push("/users");
  };
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    const { redirectToPreviousRoute } = this.state;
    const { classes } = this.props;

    if (redirectToPreviousRoute) {
      return <Redirect to={from} />;
    }

    return (
      <Grid container spacing={0} alignItems="center" justify="center" style={{ minHeight: "100vh" }}>
        <Grid item xs={4}>
          <Paper className={classes.padding}>
            <div className={classes.margin}>
              <Grid container spacing={8} alignItems="flex-end">
                <Grid item>
                  <Face />
                </Grid>
                <Grid item md={true} sm={true} xs={true}>
                  <TextField
                    id="username"
                    label="Username"
                    fullWidth
                    autoFocus
                    required
                    value={this.state.username}
                    onChange={this.handleChange("username")}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={8} alignItems="flex-end">
                <Grid item>
                  <Fingerprint />
                </Grid>
                <Grid item md={true} sm={true} xs={true}>
                  <TextField
                    id="password"
                    label="Password"
                    type="password"
                    fullWidth
                    required
                    value={this.state.password}
                    onChange={this.handleChange("password")}
                  />
                </Grid>
              </Grid>
              <Grid container justify="center" style={{ marginTop: "10px" }}>
                <Button variant="outlined" color="primary" style={{ textTransform: "none" }} onClick={this.handleClick}>
                  Login
                </Button>
              </Grid>
            </div>
          </Paper>
        </Grid>
        <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">Login Error</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">Username or Password is incorrect!</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }
}

export default withStyles(styles)(Login);
