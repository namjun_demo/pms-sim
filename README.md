## Program Installation

First, prepare any PC: Windows(tested), Linux(maybe), Mac PC(maybe) and install the following applications.

1. Install **node.js** from this [link](https://nodejs.org/en/)
2. Install **mongodb** [link](https://www.mongodb.com/)
3. Install **git** [link](https://git-scm.com/downloads)

---
## Server Configuration command

Execute git bash terminal and go to your working directory and run following commands.

```
npm install yarn -g
npm install pm2 -g
git clone https://namjun_demo@bitbucket.org/namjun_demo/pms-sim.git
cd pms-sim
yarn install
yarn build
pm2 start server/index.js
```

*If you want to check that the server process is running well, run **pm2 list** or **pm2 monitor** command. If you want to kill this process, run **pm2 delete all** command.*

## PCD/PCA configuration
1. Open Admin client page
2. Browse to PMS setting (One-way PMS)
3. Host IP should be simulator server IP
4. Port number should be 4000
5. User: admin, Password: procentric
---


## Usage

### One time configuration

First, configure PMS interface in PCA server (PCS200R)

1. Run Chrome browser and type this [URL](localhost:4000)
2. Login 'admin / procentric'
3. Click setting icon and confiure PCA server IP address
4. Add all rooms with dummy guest name

### Daily operation

Whenever guest check in / out, just click 

1. Run Chrome browser and type this [URL](localhost:4000)
2. Login 'admin / procentric'
3. Search room number
4. Click pencil icon to edit
5. Select check in or out and click save

---

## Trouble shooting

Send email to pnj1917@gmail.com
