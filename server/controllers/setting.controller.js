const Setting = require('../models/setting.model.js');

// Create and Save a new Setting
exports.create = (req, res) => {
    // Validate request
    console.log('create body= ', req.body);
    if(!req.body.procentricServerType) {
        return res.status(400).send({
            message: "procentricServerType can not be empty"
        });
    }

    // Create a Setting
    const setting = new Setting({
        procentricServerType: req.body.procentricServerType || "PCA", 
        procentricServerIp: req.body.procentricServerIp,
        procentricServerPort: req.body.procentricServerPort,
    });

    // Save Setting in the database
    setting.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Setting."
        });
    });
};

// Retrieve and return all settings from the database.
exports.findAll = (req, res) => {
    Setting.find()
    .then(settings => {
        res.send(settings);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving settings."
        });
    });
};

// Find a single setting with a settingId
exports.findOne = (req, res) => {
    Setting.findById(req.params.settingId)
    .then(setting => {
        if(!setting) {
            return res.status(404).send({
                message: "Setting not found with id " + req.params.settingId
            });            
        }
        res.send(setting);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Setting not found with id " + req.params.settingId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving setting with id " + req.params.settingId
        });
    });
};

// Update a setting identified by the settingId in the request
exports.update = (req, res) => {
    // Validate Request
    console.log('update body= ', req.body);
    // if(!req.body.settingNumber) {
    //     return res.status(400).send({
    //         message: "Setting number can not be empty"
    //     });
    // }

    // Find setting and update it with the request body
    Setting.findByIdAndUpdate(req.params.settingId, req.body
        //{
        //settingNumber: req.body.settingNumber || "999",
        //customer: req.body.customer,
        //}
    , {new: true})
    .then(setting => {
        if(!setting) {
            return res.status(404).send({
                message: "Setting not found with id " + req.params.settingId
            });
        }
        res.send(setting);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Setting not found with id " + req.params.settingId
            });                
        }
        return res.status(500).send({
            message: "Error updating setting with id " + req.params.settingId
        });
    });
};

// Delete a setting with the specified settingId in the request
exports.delete = (req, res) => {
    Setting.findByIdAndRemove(req.params.settingId)
    .then(setting => {
        if(!setting) {
            return res.status(404).send({
                message: "Setting not found with id " + req.params.settingId
            });
        }
        res.send({message: "Setting deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Setting not found with id " + req.params.settingId
            });                
        }
        return res.status(500).send({
            message: "Could not delete setting with id " + req.params.settingId
        });
    });
};