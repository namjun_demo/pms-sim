const Room = require("../models/room.model.js");
const convert = require("xml-js");

// Retrieve and return all rooms from the database.
exports.generateXML = (req, res) => {
  Room.find()
    .then(rooms => {
      const mapFunc = function(room) {
        //console.log(room.updatedAt);
        const checkoutDate = new Date(room.updatedAt);
        checkoutDate.setDate(checkoutDate.getDate() + 7);
        //console.log(checkoutDate);
        if (room.checkInOut === "Check In") {
          return {
            _attributes: { lastUpdate: "2018-02-14T00:00:09+01:00" },
            roomID: room.roomNumber,
            guest: {
              firstName: {},
              lastName: room.customer,
              // salutation: "Mr.",
              // langcode: "en_GB",
              // checkIn: room.updatedAt.toISOString().slice(0, -5) + "+01:00",
              // scheduledCheckOut: checkoutDate.toISOString().slice(0, -5) + "+01:00",
              // checkInStatus: "true"
              // firstName: {},
              // lastName: "Mr. Namjun",
              salutation: {},
              langcode: "en_GB",
              checkIn: room.updatedAt.toISOString().slice(0, -5) + "+01:00",
              scheduledCheckOut: checkoutDate.toISOString().slice(0, -5) + "+01:00",
              checkInStatus: "true"
            },
            empty: {}
          };
        } else {
          return {
            _attributes: { lastUpdate: "2018-02-14T00:00:09+01:00" },
            roomID: room.roomNumber,
            empty: {}
          };
        }
      };
      const js = {
        _declaration: { _attributes: { version: "1.0", encoding: "utf-8" } },
        PMSData: {
          _attributes: { lastUpdate: "2018-02-14T08:55:15+01:00" },
          room: rooms.map(mapFunc)
        }
      };
      const xml = convert.js2xml(js, { compact: true, fullTagEmptyElement: false, spaces: 4 });
      //console.log(js, '\n', xml);
      res.header("Content-Type", "text/xml").send(xml);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving rooms."
      });
    });
};
