const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");

// create express app
const app = express();
const PORT = process.env.PORT || 4000;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "..", "build/")));

// Configuring the database
const dbConfig = require("./config/database.config.js");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose
  .connect(
    dbConfig.url,
    {
      useNewUrlParser: true
    }
  )
  .then(() => {
    console.log("Successfully connected to the database");
  })
  .catch(err => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
  });

// define a simple route

app.get("/", (req, res) => {
  res.sendFile(path.resolve(__dirname, "..", "build", "index.html"));
});

require("./routes/room.routes.js")(app);
require("./routes/setting.routes.js")(app);
require("./routes/pms.routes.js")(app);

// listen for requests
app.listen(PORT, () => {
  console.log(`Check out the app at http://localhost:${PORT}`);
});
