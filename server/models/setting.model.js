const mongoose = require('mongoose');

const settingSchema = mongoose.Schema({
    procentricServerType: String,
    procentricServerIp: String,
    procentricServerPort: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Setting', settingSchema);