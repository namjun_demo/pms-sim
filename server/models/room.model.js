const mongoose = require('mongoose');

const RoomSchema = mongoose.Schema({
    checkInOut: String,
    roomNumber: String,
    customer: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Room', RoomSchema);