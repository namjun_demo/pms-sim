module.exports = (app) => {
    const settings = require('../controllers/setting.controller.js');

    // Create a new setting
    app.post('/settings', settings.create);

    // Retrieve all settings
    app.get('/settings', settings.findAll);

    // Retrieve a setting
    app.get('/settings/:settingId', settings.findOne);

    // Update a setting with settingId
    app.put('/settings/:settingId', settings.update);

    // Delete a setting with settingsId
    app.delete('/settings/:settingId', settings.delete);

}