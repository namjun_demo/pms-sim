module.exports = app => {
  const pms = require("../controllers/pms.controller.js");
  const basicAuth = require("express-basic-auth");

  app.use(
    basicAuth({
      users: { admin: "procentric" }
    })
  );

  // Retrieve pms xml
  app.get("/api/pcs", pms.generateXML);
};
